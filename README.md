# macosx的redis-cluster配置文件生成工具

## 依赖

- python3
- 
- docker-compose

## 安装

下载项目的代码

```bash
git clone https://gitee.com/jack_cheng/redis_cluster.git
```

安装Pipenv

```bash
brew install pipenv
```

安装python依赖的包

```bash
pipenv install
```

## 配置端口和密码

代码中默认的端口为6381-6386，默认密码为123456

```python
#####################################################################
#开始的端口
start_port=6381

#结束端口
end_port=6386

#密码
password="123456"

#####################################################################
```

## 生成配置文件

### 启动python虚拟环境

```bash
pipenv shell
```

### 生成配置文件

配置文件将会生成到redis-cluster_cnf文件夹中，执行下面命令行时候，会输出下一步要执行命令

```bash
python main.py
```